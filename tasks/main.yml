---
- name: Install libraries needed
  apt:
    name:
      - libpq-dev
      - gnupg
      - python3-psycopg2
    state: present

- name: Add apt-key
  apt_key:
    url: "https://www.postgresql.org/media/keys/{{ postgresql_apt_key }}"
    validate_certs: "{{ postgresql_validate_certs }}"
  when: postgresql_apt_key is defined

- name: Add apt repo
  apt_repository:
    repo: "deb http://apt.postgresql.org/pub/repos/apt/ {{ ansible_distribution_release }}-pgdg main"
    filename: "pgdg"
    state: present
    update_cache: yes
  when: postgresql_apt_key is defined

- name: Install PostgreSQL packages
  apt:
    name:
      - sshpass
      - postgresql-common
      - "postgresql-{{ postgresql_version }}"
      - "postgresql-contrib-{{ postgresql_version }}"
    state: present

- include: datadir.yml

- name: Copy pg_hba.conf
  template:
    src: "{{ postgresql_pghba_template_path }}"
    dest: "{{ postgresql_config_path }}/pg_hba.conf"
    owner: postgres
    group: postgres
    mode: 0640
  
- name: Restart pg(1)
  service:
    name: postgresql
    state: restarted
    enabled: yes

- name: Configure PostgreSQL settings
  lineinfile:
    dest: "{{ postgresql_config_path }}/postgresql.conf"
    regexp: "^#?{{ item.option }}.+$"
    line: "{{ item.option }} = '{{ item.value }}'"
    state: "{{ item.state | default('present') }}"
  with_items: "{{ postgresql_config_options }}"

- name: Restart pg (2)
  service:
    name: postgresql
    state: restarted
    enabled: yes

- name: Add users in PostgreSQL if needed
  postgresql_user:
    name: "{{ item.name }}"
    password: "{{ item.password }}"
    encrypted: "{{ item.encrypted | default('yes') }}"
    role_attr_flags: "{{ item.role_attr_flags | default('NOSUPERUSER') }}"
    port: "{{ postgresql_port }}"
  with_items: "{{ postgresql_users }}"

- include: databases.yml
  when: postgresql_create_db
  become: yes
  become_user: postgres

- include: replication.yml
  when:
   - postgresql_replication
   - postgresql_group_master in groups
   - postgresql_group_replica in groups
